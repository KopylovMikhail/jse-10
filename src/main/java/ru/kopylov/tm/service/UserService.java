package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.IUserRepository;
import ru.kopylov.tm.api.service.IUserService;
import ru.kopylov.tm.entity.User;

import java.util.List;

@NoArgsConstructor
public final class UserService extends AbstractService implements IUserService {

    @NotNull
    private IUserRepository userRepository = (IUserRepository) abstractRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean merge(@Nullable final User user) {
        if (user == null) return false;
        if (user.getId() == null || user.getId().isEmpty()) return false;
        if (user.getLogin() == null || user.getLogin().isEmpty()) return false;
        if (user.getPassword() == null || user.getPassword().isEmpty()) return false;
        if (userRepository.findOne(user.getId()) == null) return false;
        userRepository.merge(user);
        return true;
    }

    public boolean persist(@Nullable final User user) {
        if (user == null) return false;
        return !user.equals(userRepository.persist(user));
    }

    @Nullable
    public User findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return userRepository.findOne(id);
    }

    @NotNull
    public List<User> findAll() {
        return userRepository.findAll();
    }

}
