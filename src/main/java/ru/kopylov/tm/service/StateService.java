package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.IStateRepository;
import ru.kopylov.tm.api.service.IStateService;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.User;

import java.util.Map;

@NoArgsConstructor
public final class StateService extends AbstractService implements IStateService {

    @NotNull
    private IStateRepository stateRepository = (IStateRepository) abstractRepository;

    public StateService(@NotNull final IStateRepository stateRepository) {
        this.stateRepository = stateRepository;
    }

    @NotNull
    public User getCurrentUser() {
        return stateRepository.getCurrentUser();
    }

    public void setCurrentUser(@NotNull final User currentUser) {
        stateRepository.setCurrentUser(currentUser);
    }

    @NotNull
    public Map<String, AbstractCommand> getCommands() {
        return stateRepository.getCommands();
    }

    public void registry(@NotNull final AbstractCommand command) {
        stateRepository.registry(command);
    }

}
