package ru.kopylov.tm.command.data.xml;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.constant.DataPath;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Root;
import ru.kopylov.tm.entity.Task;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.nio.file.Files;
import java.util.List;

@NoArgsConstructor
public final class SaveJaxbXmlCommand extends AbstractCommand {

    @Override
    public @NotNull String getName() {
        return "data-xml-save-jaxb";
    }

    @Override
    public @NotNull String getDescription() {
        return "Saving a subject area using JAX-B in xml-format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML by JAX-B SAVE]");
        @Nullable final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
        @NotNull final List<Project> projects = bootstrap.getProjectService().findAll(currentUserId);
        @NotNull final List<Task> tasks = bootstrap.getTaskService().findAll(currentUserId);
        @NotNull final Root root = new Root();
        root.setProjects(projects);
        root.setTasks(tasks);
        @NotNull final File file = new File(DataPath.PATH_JAXB_XML);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Root.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(root, file);
        System.out.println("[OK]");
    }

}
