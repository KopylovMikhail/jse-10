package ru.kopylov.tm.command.data.binary;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.constant.DataPath;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Task;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.List;

@NoArgsConstructor
public final class LoadSerializeCommand extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "data-bin-load";
    }

    @Override
    public @NotNull String getDescription() {
        return "Load a subject area using serialization.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BIN LOAD]");
        @NotNull final File file = new File(DataPath.PATH_BIN);
        @NotNull final FileInputStream fileInputStream = new FileInputStream(file);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        loadProjects(objectInputStream.readObject());
        loadTasks(objectInputStream.readObject());
        objectInputStream.close();
        fileInputStream.close();
        System.out.println("[OK]");
    }

    private void loadProjects(@Nullable final Object object) {
        if (!(object instanceof List<?>)) return;
        if (((List) object).isEmpty()) return;
        if (!(((List) object).get(0) instanceof Project)) return; //если первый объект листа не принадлежит типу Project, то выход из метода
        @NotNull final List<Project> projects = (List<Project>) object;
        for (Project project : projects) {
            bootstrap.getProjectService().persist(project);
        }
    }

    private void loadTasks(@Nullable final Object object) {
        if (!(object instanceof List<?>)) return;
        if (((List) object).isEmpty()) return;
        if (!(((List) object).get(0) instanceof Task)) return; //если первый объект листа не принадлежит типу Task, то выход из метода
        @NotNull final List<Task> tasks = (List<Task>) object;
        for (Task task : tasks) {
            bootstrap.getTaskService().persist(task);
        }
    }

}
