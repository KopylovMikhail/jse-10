package ru.kopylov.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.constant.DataPath;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Root;
import ru.kopylov.tm.entity.Task;

import java.io.File;
import java.util.List;

@NoArgsConstructor
public final class SaveFasterxmlJsonCommand extends AbstractCommand {

    @Override
    public @NotNull String getName() {
        return "data-json-save";
    }

    @Override
    public @NotNull String getDescription() {
        return "Saving a subject area using FASTERXML in json-format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON by FASTERXML SAVE]");
        @Nullable final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
        @NotNull final List<Project> projects = bootstrap.getProjectService().findAll(currentUserId);
        @NotNull final List<Task> tasks = bootstrap.getTaskService().findAll(currentUserId);
        @NotNull final Root root = new Root();
        root.setProjects(projects);
        root.setTasks(tasks);
        @NotNull final File file = new File(DataPath.PATH_FASTERXML_JSON);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, root);
        System.out.println("[OK]");
    }

}
