package ru.kopylov.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.constant.DataPath;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Root;
import ru.kopylov.tm.entity.Task;

import java.io.File;

@NoArgsConstructor
public final class LoadFasterxmlJsonCommand extends AbstractCommand {

    @Override
    public @NotNull String getName() {
        return "data-json-load";
    }

    @Override
    public @NotNull String getDescription() {
        return "Load a subject area using FASTERXML from json-format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON by FASTERXML LOAD]");
        @NotNull final File file = new File(DataPath.PATH_FASTERXML_JSON);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Root root = objectMapper.readValue(file, Root.class);
        for (Project project : root.getProjects()) {
            bootstrap.getProjectService().persist(project);
        }
        for (Task task : root.getTasks()) {
            bootstrap.getTaskService().persist(task);
        }
        System.out.println("[OK]");
    }

}
