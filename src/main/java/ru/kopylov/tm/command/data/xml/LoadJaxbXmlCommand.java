package ru.kopylov.tm.command.data.xml;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.constant.DataPath;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Root;
import ru.kopylov.tm.entity.Task;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

@NoArgsConstructor
public final class LoadJaxbXmlCommand extends AbstractCommand {

    @Override
    public @NotNull String getName() {
        return "data-xml-load-jaxb";
    }

    @Override
    public @NotNull String getDescription() {
        return "Load a subject area using JAX-B from xml-format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML by JAX-B LOAD]");
        @NotNull final File file = new File(DataPath.PATH_JAXB_XML);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Root.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final Root root = (Root) unmarshaller.unmarshal(file);
        for (Project project : root.getProjects()) {
            bootstrap.getProjectService().persist(project);
        }
        for (Task task : root.getTasks()) {
            bootstrap.getTaskService().persist(task);
        }
        System.out.println("[OK]");
    }

}
