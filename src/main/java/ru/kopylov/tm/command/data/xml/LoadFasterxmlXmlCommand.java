package ru.kopylov.tm.command.data.xml;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.constant.DataPath;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Root;
import ru.kopylov.tm.entity.Task;

import java.io.File;

@NoArgsConstructor
public final class LoadFasterxmlXmlCommand extends AbstractCommand {

    @Override
    public @NotNull String getName() {
        return "data-xml-load";
    }

    @Override
    public @NotNull String getDescription() {
        return "Load a subject area using FASTERXML from xml-format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML by FASTERXML LOAD]");
        @NotNull final File file = new File(DataPath.PATH_FASTERXML_XML);
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final Root root = xmlMapper.readValue(file, Root.class);
        for (Project project : root.getProjects()) {
            bootstrap.getProjectService().persist(project);
        }
        for (Task task : root.getTasks()) {
            bootstrap.getTaskService().persist(task);
        }
        System.out.println("[OK]");
    }

}
