package ru.kopylov.tm.command.data.binary;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.constant.DataPath;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Task;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.util.List;

@NoArgsConstructor
public final class SaveSerializeCommand extends AbstractCommand {

    @Override
    public @NotNull String getName() {
        return "data-bin-save";
    }

    @Override
    public @NotNull String getDescription() {
        return "Saving a subject area using serialization.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BIN SAVE]");
        @Nullable final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
        @NotNull final List<Project> projects = bootstrap.getProjectService().findAll(currentUserId);
        @NotNull final List<Task> tasks = bootstrap.getTaskService().findAll(currentUserId);
        @NotNull final File file = new File(DataPath.PATH_BIN);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(projects);
        objectOutputStream.writeObject(tasks);
        objectOutputStream.close();
        fileOutputStream.close();
        System.out.println("[OK]");
    }

}
