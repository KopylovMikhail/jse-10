package ru.kopylov.tm.command.data.json;

import lombok.NoArgsConstructor;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.constant.DataPath;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Root;
import ru.kopylov.tm.entity.Task;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.nio.file.Files;
import java.util.List;

@NoArgsConstructor
public final class SaveJaxbJsonCommand extends AbstractCommand {

    @Override
    public @NotNull String getName() {
        return "data-json-save-jaxb";
    }

    @Override
    public @NotNull String getDescription() {
        return "Saving a subject area using JAX-B in json-format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON by JAX-B SAVE]");
        @Nullable final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
        @NotNull final List<Project> projects = bootstrap.getProjectService().findAll(currentUserId);
        @NotNull final List<Task> tasks = bootstrap.getTaskService().findAll(currentUserId);
        @NotNull final Root root = new Root();
        root.setProjects(projects);
        root.setTasks(tasks);
        @NotNull final File file = new File(DataPath.PATH_JAXB_JSON);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Root.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(root, file);
        System.out.println("[OK]");
    }

}
