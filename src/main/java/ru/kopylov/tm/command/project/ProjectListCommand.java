package ru.kopylov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.util.CommandUtil;
import ru.kopylov.tm.util.ProjectComparator;

import java.io.IOException;
import java.util.List;

@NoArgsConstructor
public final class ProjectListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
        @NotNull final List<Project> projectList = bootstrap.getProjectService().findAll(currentUserId);
        System.out.println("[SORTED BY CREATE]");
        CommandUtil.printProjectListWithParam(projectList);
        System.out.println("\nFOR SORT LIST TYPE ANY OF THESE COMMANDS:" +
                "\n     by-date-start" +
                "\n     by-date-finish" +
                "\n     by-state" +
                "\nFOR RETURN TO MAIN MENU PRESS [ENTER]\n");
        @Nullable final String sortCommand = bootstrap.getTerminalService().getReadLine();
        if ("by-date-start".equals(sortCommand)) {
            System.out.println("[SORTED BY DATE START]");
            projectList.sort(ProjectComparator.byDateStart);
            CommandUtil.printProjectListWithParam(projectList);
        }
        if ("by-date-finish".equals(sortCommand)) {
            System.out.println("[SORTED BY DATE FINISH]");
            projectList.sort(ProjectComparator.byDateFinish);
            CommandUtil.printProjectListWithParam(projectList);
        }
        if ("by-state".equals(sortCommand)) {
            System.out.println("[SORTED BY STATE]");
            projectList.sort(ProjectComparator.byState);
            CommandUtil.printProjectListWithParam(projectList);
        }
    }

}
