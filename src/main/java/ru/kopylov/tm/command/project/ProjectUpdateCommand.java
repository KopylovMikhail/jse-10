package ru.kopylov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.enumerated.State;
import ru.kopylov.tm.util.CommandUtil;
import ru.kopylov.tm.util.DateUtil;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
public final class ProjectUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-update";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update selected project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT UPDATE]\n" +
                "ENTER EXISTING PROJECT NUMBER:");
        @Nullable final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
        @NotNull final List<Project> projectList = bootstrap.getProjectService().findAll(currentUserId);
        CommandUtil.printProjectList(projectList);
        final int projectNumber = Integer.parseInt(bootstrap.getTerminalService().getReadLine());
        @NotNull final Project project = projectList.get(projectNumber - 1);
        System.out.println("ENTER NEW PROJECT NAME OR PRESS [ENTER] TO SKIP:");
        @Nullable String terminalText = bootstrap.getTerminalService().getReadLine();
        if (terminalText != null && !terminalText.isEmpty())
            project.setName(terminalText);
        System.out.println("ENTER PROJECT DESCRIPTION OR PRESS [ENTER] TO SKIP:");
        terminalText = bootstrap.getTerminalService().getReadLine();
        if (terminalText != null && !terminalText.isEmpty())
            project.setDescription(terminalText);
        System.out.println("ENTER PROJECT DATE START (DD.MM.YYYY) OR PRESS [ENTER] TO SKIP:");
        terminalText = bootstrap.getTerminalService().getReadLine();
        if (terminalText != null && !terminalText.isEmpty()) {
            @NotNull final Date dateStart = DateUtil.stringToDate(terminalText);
            project.setDateStart(dateStart);
        }
        System.out.println("ENTER PROJECT DATE FINISH (DD.MM.YYYY) OR PRESS [ENTER] TO SKIP:");
        terminalText = bootstrap.getTerminalService().getReadLine();
        if (terminalText != null && !terminalText.isEmpty()) {
            @NotNull final Date dateFinish = DateUtil.stringToDate(terminalText);
            project.setDateFinish(dateFinish);
        }
        int count = 1;
        System.out.println("ENTER PROJECT STATE NUMBER OR PRESS [ENTER] TO SKIP:");
        for (State state : State.values()) {
            System.out.println(count++ + ". " + state.getDisplayName());
        }
        terminalText = bootstrap.getTerminalService().getReadLine();
        if (terminalText != null && !terminalText.isEmpty()) {
            final int stateNumber = Integer.parseInt(terminalText);
            project.setState(State.values()[stateNumber-1]);
        }
        if (bootstrap.getProjectService().merge(project))
            System.out.println("[PROJECT HAS BEEN UPDATED]\n");
        else System.out.println("SUCH A PROJECT DOES NOT EXIST OR NAME IS EMPTY.\n");
    }

}
