package ru.kopylov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
public abstract class AbstractRepository<T extends AbstractEntity> {

    @NotNull
    protected final Map<String, T> entityMap = new LinkedHashMap<>();

    public void merge(@NotNull final T entity) {
        entityMap.put(entity.getId(), entity);
    }

    @Nullable
    T persist(@NotNull final T entity) {
        return entityMap.putIfAbsent(entity.getId(), entity);
    }

    @NotNull
    public List<T> findAll() {
        return new ArrayList<>(entityMap.values());
    }

    public abstract boolean remove(@NotNull final String entityName);

    public void removeAll() {
        entityMap.clear();
    }

}
