package ru.kopylov.tm;

import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.context.Bootstrap;

import java.util.Set;

/**
 * @author Mikhail Kopylov
 * task/project manager
 */

public final class Application {

    @NotNull
    private static final Set<Class<? extends AbstractCommand>> CLASSES =
            new Reflections("ru.kopylov.tm.command").getSubTypesOf(AbstractCommand.class);

    public static void main(String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(CLASSES);
    }

}
