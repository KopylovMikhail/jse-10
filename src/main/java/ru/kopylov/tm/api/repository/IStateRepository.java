package ru.kopylov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.User;

import java.util.Map;

public interface IStateRepository {

    @NotNull
    User getCurrentUser();

    void setCurrentUser(@NotNull User currentUser);

    @NotNull
    Map<String, AbstractCommand> getCommands();

    void registry(@NotNull AbstractCommand command);

}
