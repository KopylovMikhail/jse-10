package ru.kopylov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.entity.User;

import java.util.List;

public interface IUserService {

    boolean merge(@Nullable User user);

    boolean persist(@Nullable User user);

    @Nullable
    User findOne(@Nullable String id);

    @NotNull
    List<User> findAll();

}
