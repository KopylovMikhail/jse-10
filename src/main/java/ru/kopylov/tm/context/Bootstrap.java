package ru.kopylov.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.*;
import ru.kopylov.tm.api.service.*;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.TypeRole;
import ru.kopylov.tm.repository.*;
import ru.kopylov.tm.service.*;
import ru.kopylov.tm.util.HashUtil;

import java.util.Date;
import java.util.Set;

@Getter
@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskOwnerRepository taskOwnerRepository = new TaskOwnerRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IStateRepository stateRepository = new StateRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository, taskRepository, taskOwnerRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository, taskOwnerRepository);

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IStateService stateService = new StateService(stateRepository);

    @NotNull
    private final ITerminalService terminalService = new TerminalService();

    public void init(@NotNull Set<Class<? extends AbstractCommand>> classes) throws Exception {
        try {
            for (@NotNull Class clazz : classes) {
                commandRegistry(clazz);
            }
            userInit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        System.out.println("*** WELCOME TO TASK MANAGER ***\n");
        @Nullable String command = "";
        do {
            try {
                command = terminalService.getReadLine();
                execute(command);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                if (e.getMessage() == null) System.out.println(e.toString());
            }
        } while (!"exit".equals(command));
    }

    private void commandRegistry(@NotNull Class clazz) throws Exception {
        if (AbstractCommand.class.isAssignableFrom(clazz)) {
            @NotNull final AbstractCommand command = (AbstractCommand) clazz.newInstance();
            command.setBootstrap(this);
            stateService.registry(command);
        }
    }

    private void execute(@Nullable final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        if (checkPermission(command)) {
            System.out.println("You don't have enough permissions.\n");
            return;
        }
        @Nullable final AbstractCommand abstractCommand = stateService.getCommands().get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

    private boolean checkPermission(@NotNull final String command) {
        final boolean contentProject = command.contains("project");
        final boolean contentTask = command.contains("task");
        final boolean contentUser = command.contains("user");
        final boolean userNotAuth = (stateService.getCurrentUser().getRole() == null);
        return  ((contentProject || contentTask || contentUser) & userNotAuth);
    }

    private void userInit() throws Exception {
        @NotNull final User admin = new User();
        admin.setLogin("admin");
        admin.setPassword(HashUtil.hash("111111"));
        admin.setRole(TypeRole.ADMIN);
        @NotNull final User user = new User();
        user.setLogin("user");
        user.setRole(TypeRole.USER);
        user.setPassword(HashUtil.hash("222222"));
        userService.persist(admin);
        userService.persist(user);
    }

}
