package ru.kopylov.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.entity.Task;

import java.util.Comparator;

public final class TaskComparator {

    @NotNull
    public final static Comparator<Task> byDateStart = new Comparator<Task>() {
        @Override
        public final int compare(@NotNull Task t1, @NotNull Task t2) {
            return t1.getDateStart().compareTo(t2.getDateStart());
        }
    };

    @NotNull
    public final static Comparator<Task> byDateFinish = new Comparator<Task>() {
        @Override
        public final int compare(@NotNull Task t1, @NotNull Task t2) {
            return t1.getDateFinish().compareTo(t2.getDateFinish());
        }
    };

    @NotNull
    public final static Comparator<Task> byState = new Comparator<Task>() {
        @Override
        public final int compare(@NotNull Task t1, @NotNull Task t2) {
            return t1.getState().compareTo(t2.getState());
        }
    };

}
